﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Rumba : MonoBehaviour {


	GameObject gameManager;

	public float speed = 4.0f;

	// Use this for initialization
	void Start () {
		// 移動する向きを求める
		Vector2 direction = new Vector2 (1, 2).normalized;
		//Vector2 direction = transform.up;
		// 移動する向きとスピードを代入する
		GetComponent<Rigidbody2D>().velocity = direction * speed;

	
		gameManager = GameObject.Find("GameManager");	
	}

	void gameFinish ()
	{
		Destroy (gameObject);
	}

	// Update is called once per frame
	void Update () {
		// オブジェクトを前方方向に z 軸に沿って 1 メートル/秒で移動させる
		//transform.position += Vector3(Time.deltaTime,0,0);
		//Y軸で回転させてみる
		transform.Rotate(0,0,1);

		if (FindObjectOfType<GameManager> ().time == 10) {
			Vector2 direction = new Vector2 (0.3f, 1.2f).normalized;
			GetComponent<Rigidbody2D>().velocity = direction * 8.0f;
		}


	}
	//	void OnCollisionEnter2D(Collision2D col)
	void OnTriggerEnter2D (Collider2D col)
	{
		//Debug.Log(col.gameObject.name);
		if (col.gameObject.name == "Box"){
			print ("atatta");
			col.gameObject.name = "BoxOpend";
			FindObjectOfType<GameManager>().IncrementRumbaScore(1);
			GetComponent<AudioSource>().Play ();
			//gameManager.SendMessage("IncrementRumbaScore", 1);
			//gameManager.SendMessage("Test");
			Destroy(col.gameObject);
		}

	}
}