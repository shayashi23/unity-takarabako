﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Rigidbody2D), typeof(BoxCollider2D))]

public class Box : MonoBehaviour {

	SpriteRenderer MainSpriteRenderer;
	// publicで宣言し、inspectorで設定可能にする
	public Sprite CloseSprite;
	public Sprite OpenSprite;

	public int hp = 5;


	void Start () {
		MainSpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
		var time = FindObjectOfType<GameManager> ().time;
		Destroy (gameObject, time);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void Test(){
		print ("test!!");
	}

	// 何かしらのタイミングで呼ばれる
	void Open()
	{
		// SpriteRenderのspriteを設定済みの他のspriteに変更
		// 例) HoldSpriteに変更

		MainSpriteRenderer.sprite = OpenSprite;
	}

}
