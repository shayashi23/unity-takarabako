﻿using UnityEngine;
using System.Collections;

public class BoxManager : MonoBehaviour {

	public GameObject box;
	public float box_wait = 0.02f;

	GameObject background;

	void Start ()
	{
		background = GameObject.Find("Background");
		StartCoroutine ("Make");
	}

	IEnumerator Make(){
		while (true) {
			var obj = Instantiate(box, transform.position, transform.rotation) as GameObject;
			obj.name = box.name; // (Clone対策)

			// TODO ここ、どうやって位置をつかむのか不明
			//obj.transform.position = new Vector3(Random.Range (-3, 3), Random.Range (-2, 3), 0);





			// ワールド座標をビューポートから取得
			//Vector2 pos = Camera.main.ViewportToWorldPoint(new Vector2(Random.Range (0.0f, 1.0f), Random.Range (0.0f, 1.0f)));



			// 背景のスケール
			Vector2 scale = background.transform.localScale;

			// 背景のスケールから取得
			Vector2 min = scale * -0.5f;
			Vector2 max = scale * 0.5f;

			// プレイヤーの座標を取得
			Vector3 pos = transform.position;

			// プレイヤーの位置が画面内に収まるように制限をかける
			pos.x = Random.Range (min.x, max.x);
			pos.y = Random.Range (min.y, max.y);

			// 制限をかけた値をプレイヤーの位置とする
			transform.position = pos;

			yield return new WaitForSeconds (box_wait);
		}		
	}
	void Update () {

	}
}
