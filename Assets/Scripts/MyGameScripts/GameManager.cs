﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public int player_score = 0;
	public int rumba_score = 0;
	public bool isPlaying = false;
	public GUIText playerScore;
	public GUIText rumbaScore;
	public GUIText timerText;
	public GUIText titleText;
	private static int timeLimit = 30;
	public int time;


	// プレハブをアタッチする余地を与える
	public GameObject boxManager;
	public GameObject bgm;
	public GameObject rumba;


	// Use this for initialization
	void Start () {
		//rumba = GameObject.Find("Rumba");
		//rumba = GameObject.Find("Rumba");
	}

	void Test(){
		print ("game manager test");
	}
	public void IncrementPlayerScore(int point){
		player_score = player_score + point;
		playerScore.text = "YOU : " + player_score.ToString ();
	}

	public void IncrementRumbaScore(int point){
		rumba_score = rumba_score + point;
		rumbaScore.text = "ルンバ : " + rumba_score.ToString ();
	}
	public void UpdateTimer(int time){
		timerText.text = "TIME : " + time.ToString ();
	}

	public void UpdateTitle(string str){
		titleText.text = str;
	}

	void GameStart(){
		isPlaying = true;

		player_score = 0;
		rumba_score = 0;
		playerScore.text = "YOU : 0";

		StartCoroutine ("GameStartWait2");


	}
	IEnumerator GameStartWait2(){
		yield return new WaitForSeconds (1);
		time = timeLimit;
		MakeInstance (boxManager);

		//titleText.SetActive (false); //つかえねー.. TODO テキストを消すときどうする
		UpdateTitle("");
		UpdateTimer(time);
		StartCoroutine ("Countdown");
		StartCoroutine ("RumbaAppear");
	}

	void MakeInstance(GameObject obj){
		GameObject instance = (GameObject)Instantiate(obj);
		instance.name = obj.name;
		Destroy (instance, time);
	}
	/*
	void DestroyInstance(string a){
		FindObjectOfType<a> ().Destroy ();
	}
	*/

	void GameEnd(){

		//DestroyInstance ("boxManager");

		//boxManager.gameFinish();
		//rumba.gameFinish();
//		Destroy (bgm);
//		Destroy (rumba);

	
		StopCoroutine ("Countdown");

		if (player_score > rumba_score) {
			UpdateTitle ("YOU WIN");
		} else if (player_score < rumba_score) {
			UpdateTitle (player_score + " vs " + rumba_score + "でルンバの勝ち");
		} else {
			UpdateTitle ("ひきわけ");		
		}

		StartCoroutine ("GameStartWait");

	}

	IEnumerator RumbaAppear(){
		yield return new WaitForSeconds (4.5f);
		rumbaScore.text = "ルンバ : 0";
		MakeInstance (rumba);
		MakeInstance (bgm);
	}

	IEnumerator GameStartWait(){
		yield return new WaitForSeconds (2);
		isPlaying = false;
		UpdateTitle ("ARE YOU READY?");
	}

	IEnumerator Countdown(){
		while (true) {
			yield return new WaitForSeconds (1);
			time -= 1;
			print (time);
			UpdateTimer (time);
			if (time <= 0) {
				GameEnd ();
			}
		}

	}

	void Update () {

		if (!isPlaying && Input.GetMouseButtonDown (0)) {
			GameStart ();
		}

		// クリック検出
		if (isPlaying && Input.GetMouseButtonDown(0)) {
			Vector3    aTapPoint   = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Collider2D aCollider2d = Physics2D.OverlapPoint(aTapPoint);

			if (aCollider2d) {
				GameObject obj = aCollider2d.transform.gameObject;

				if (obj.name == "Box") {
					var boxInfo = obj.GetComponent<Box> ();
					boxInfo.hp = boxInfo.hp - 1;
					if (boxInfo.hp <= 0) {
						obj.SendMessage("Open");
						IncrementPlayerScore (1);
						obj.name = "BoxOpened";
						Destroy (obj, 1.0f);
					} else {
						//AudioSource audio = obj.GetComponent<AudioSource>();
						//audio.Play ();//ならねー。
						obj.GetComponent<AudioSource>().Play ();
						//print ("remain" + boxInfo.hp);					
					}
				}
			}
		}
	}
}
