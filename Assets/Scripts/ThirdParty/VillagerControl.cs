﻿/*
The MIT License (MIT)
 
Copyright (c) 2014 Kotonaga
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using UnityEngine;
using System.Collections;

public class VillagerControl : CharacterControl {
	private float m_defaultSpeed;

	void Start () {
		this.m_defaultSpeed = this.m_speed;
		StartCoroutine("Walk");
	}

	private IEnumerator Walk(){
		for(;;){
			this.m_walking = true;
			this.m_speed = this.m_defaultSpeed * Random.Range (0.5f,1f);
			this.m_directionX = Random.Range (-1f,1f);
			this.m_directionY = Random.Range (-1f,1f);
			yield return new WaitForSeconds(Random.Range(0f,5f));
			this.Wait();
			yield return new WaitForSeconds(Random.Range(0f,5f));
		}
	}

	private void Wait(){
		this.m_walking = false;
	}
}