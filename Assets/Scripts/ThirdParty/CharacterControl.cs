﻿/*
The MIT License (MIT)

Copyright (c) 2014 Kotonaga

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using UnityEngine;

public class CharacterControl : MonoBehaviour {
	public float m_speed;
	protected Animator m_animator;
	protected float m_directionX = 0;
	protected float m_directionY = 0;
	protected bool m_walking = false;

	protected void Awake() {
		this.m_animator = GetComponent<Animator>();
	}

	protected void Update () {
		if(this.m_animator){        
			if(this.m_walking){
				transform.Translate(new Vector3(this.m_directionX, this.m_directionY, 0) * Time.deltaTime * m_speed);
				transform.position = new Vector3(transform.position.x,transform.position.y,transform.position.y / 1000f);
			}
			this.m_animator.SetFloat("DirectionX", this.m_directionX);
			this.m_animator.SetFloat("DirectionY", this.m_directionY);
			this.m_animator.SetBool("Walking", this.m_walking);
		}
	}
}